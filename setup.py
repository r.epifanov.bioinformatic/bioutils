# Copyrights rostepifanov.ru
#   Author: Epifanov Rostislav
#   Created: 10/12/2019

import setuptools
import pathlib

requirements_path = pathlib.Path.cwd() / 'requirements.list'

with open(requirements_path, 'r') as requirements_file:
     requirements = requirements_file.read()

requirements_list = requirements.split('\n')


setuptools.setup(
    name='bioutils',
    version='0.0.0',
    description='',
    author='rostepifanov.ru',
    author_email='info@rostepifanov.ru',
    url='',
    license='Proprietary',
    packages=setuptools.find_packages(exclude=['tests', 'tests.*']),
    python_requires='>=3.6',
    install_requires=requirements,
    zip_safe=False,
)

