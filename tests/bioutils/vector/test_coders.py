# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 23/03/2020


import pytest


from bioutils.vector import ( encode_dissociation, decode_dissociation,
                            encode_sequence, decode_sequence )


@pytest.mark.coders
def test_dissociation():
    types = ['CID', 'HCD']

    for type_ in types:
        code = encode_dissociation(type_)
        decoded_type = decode_dissociation(code)

        assert type_ == decoded_type


@pytest.mark.coders
def test_sequence():
    sequence = 'PEPTIDE'

    code = encode_sequence(sequence)
    assert len(code) == len(sequence)

    decoded_sequence = decode_sequence(code)
    assert decoded_sequence == sequence


@pytest.mark.coders
def test_sequence_fixed_size():
    sequence = 'PEPTIDE'

    code = encode_sequence(sequence, size=20)
    assert len(code) == 20

    decoded_sequence = decode_sequence(code)
    assert decoded_sequence == sequence
