# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 23/03/2020


import pytest


from bioutils.vector import ( get_vectored_spectrum )


@pytest.mark.convertors
def test_get_vectored_spectrum():
    info = { 'sequence': 'AAA',
             'spectrum': {
                 '1+': [ ('b1', 10),
                         ('b2', 20),
                         ('y1', 30),                         ('y1', 30),
                         ('y2', 40),
                       ]
             }
    }

    b_intensity, y_intensity, imask = get_vectored_spectrum(info)

    assert b_intensity.size == 2
    assert b_intensity[0] == 10
    assert b_intensity[1] == 20

    assert y_intensity.size == 2
    assert y_intensity[0] == 40
    assert y_intensity[1] == 30

    assert imask.size == 2
    assert imask[0] == 1
    assert imask[1] == 1

@pytest.mark.convertors
def test_get_vectored_spectrum_sized():
    info = { 'sequence': 'AAA',
             'spectrum': {
                 '1+': [ ('b1', 10),
                         ('b2', 20),
                         ('y1', 30),                         ('y1', 30),
                         ('y2', 40),
                       ]
             }
    }

    b_intensity, y_intensity, imask = get_vectored_spectrum(info, vsize=3)

    assert b_intensity.size == 3
    assert b_intensity[0] == 10
    assert b_intensity[1] == 20
    assert b_intensity[2] == 0

    assert y_intensity.size == 3
    assert y_intensity[0] == 40
    assert y_intensity[1] == 30
    assert y_intensity[2] == 0

    assert imask.size == 3
    assert imask[0] == 1
    assert imask[1] == 1
    assert imask[2] == 0
