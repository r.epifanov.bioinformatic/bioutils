# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 09/02/2020

import pytest


from bioutils.io.spectra import mgf_loader
from bioutils.structures import SpectrumObject


expected_load_one = [
    ( 175.116 ,     947, '' , 0.),
    ( 290.143 ,   11015, '' , 0.),
    ( 418.202 ,   17906, '' , 0.),
    ( 531.286 ,   40608, '' , 0.),
    ( 660.328 ,   44008, '' , 0.),
    ( 731.366 ,   60848, '' , 0.),
    ( 802.403 ,  105360, '' , 0.),
    ( 873.44  ,   10000, '' , 0.),
]


@pytest.mark.mgf_loader
def test_load_single():
    spectrumobject = SpectrumObject()

    with open('./data/single.mgf') as f:
        mgf_loader.load(spectrumobject, f)

    assert spectrumobject['sequence'] == 'AAAAELQDR'
    assert spectrumobject['charge'] == 2
    assert spectrumobject['MW'] == 925.49146
    assert spectrumobject['num peaks'] == 8

    for loaded, expected in zip(spectrumobject['peaks'], expected_load_one):
        assert loaded['mz'] == pytest.approx(expected[0])
        assert loaded['I'] == pytest.approx(expected[1])
        # assert loaded['type'] == expected[2]
        # assert loaded['ppm'] == pytest.approx(expected[3])


expected_load_one_annotated = [
    ( 201.124 ,   37.820, 'b2'         , 0.),
    ( 396.225 ,  560.135, 'b4-H2O'     , 0.),
    ( 531.314 ,    2.137, 'b5+H2O'     , 0.),
    ( 572.368 ,  110.747, 'y5-H2O-NH3' , 0.),
    ( 624.336 ,  250.205, 'b6-NH3'     , 0.),
    ( 641.362 ,  952.879, 'b6'         , 0.),
    ( 659.373 ,    1.031, 'b6+H2O'     , 0.),
    ( 700.427 ,  303.159, 'y6-H2O-NH3' , 0.),
    ( 717.453 , 1290.102, 'y6-H2O'     , 0.),
    ( 718.437 , 1619.824, 'y6-NH3'     , 0.),
    ( 735.464 , 5328.900, 'y6'         , 0.),
    ( 736.469 ,  117.576, ''           , 0.),
    ( 760.532 ,   75.263, 'a7'         , 0.),
    ( 770.516 , 2064.104, 'b7-H2O'     , 0.),
]


@pytest.mark.mgf_loader
def test_load_single_annotated():
    spectrumobject = SpectrumObject()

    with open('./data/single_annotated.mgf') as f:
        mgf_loader.load(spectrumobject, f)

    assert spectrumobject['comment'] == 'TVNVVQFEPSK'
    assert spectrumobject['charge'] == 1
    assert spectrumobject['MW'] == 1247.656
    assert spectrumobject['num peaks'] == 14

    for loaded, expected in zip(spectrumobject['peaks'], expected_load_one_annotated):
        assert loaded['mz'] == pytest.approx(expected[0])
        assert loaded['I'] == pytest.approx(expected[1])
        # assert loaded['type'] == expected[2]
        # assert loaded['ppm'] == pytest.approx(expected[3])
