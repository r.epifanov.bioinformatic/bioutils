# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 14/01/2020

import pytest


from bioutils.io.spectra import msp_loader
from bioutils.structures import SpectrumObject


expected_load_one = [
    ( 129.1017 ,   20123, 'y1-H2O' ,  5.34),
    ( 261.155  ,   20039, 'y3'     ,  3.95),
    ( 285.1555 ,   33470, 'b4'     ,  1.69),
    ( 356.1928 ,   56386, 'b5'     ,  0.45),
    ( 419.2261 ,   51091, 'y5'     , -2.71),
    ( 427.23   ,   86726, 'b6'     , -0.1 ),
    ( 490.2621 ,   50110, 'y6'     , -0.24),
    ( 498.2646 ,  161044, 'b7'     ,  4.72),
    ( 551.2904 ,   10362, 'b8-H2O' ,  5.5 ),
    ( 561.2968 ,   81368, 'y7'     ,  4.  ),
    ( 569.303  ,  164640, 'b8'     ,  1.79),
    ( 632.3354 ,  113617, 'y8'     ,  0.97),
    ( 640.3428 ,  206076, 'b9'     , -2.86),
    ( 703.3734 ,  158764, 'y9'     , -0.5 ),
    ( 711.3784 ,  147643, 'b10'    , -0.59),
    ( 764.4056 ,   14436, 'b11-H2O', -0.19),
    ( 774.4088 ,  166487, 'y10'    ,  1.61),
    ( 782.4136 ,  165440, 'b11'    ,  3.02),
    ( 845.4481 ,  189084, 'y11'    , -0.14),
    ( 853.4529 ,  124112, 'b12'    ,  0.07),
    ( 916.4818 ,  194421, 'y12'    ,  3.55),
    ( 924.491  ,   92042, 'b13'    , -1.05),
    ( 987.522  ,  196170, 'y13'    , -0.03),
    ( 995.5287 ,   53180, 'b14'    , -1.76),
    (1058.5569 ,  144261, 'y14'    ,  2.  ),
    (1129.5986 ,  101202, 'y15'    , -2.33),
    (1200.634  ,   48496, 'y16'    , -0.86),
]


@pytest.mark.msp_loader
def test_load_single():
    spectrumobject = SpectrumObject()

    with open('./data/single.msp') as f:
        msp_loader.load(spectrumobject, f)

    assert spectrumobject['sequence'] == 'AAAAAAAAAAAAAAAASAGGK'
    assert spectrumobject['charge'] == 2
    assert spectrumobject['MW'] == 1554.8117
    assert spectrumobject['num peaks'] == 27

    for loaded, expected in zip(spectrumobject['peaks'], expected_load_one):
        assert loaded['mz'] == pytest.approx(expected[0])
        assert loaded['I'] == pytest.approx(expected[1])
        assert loaded['type'] == expected[2]
        assert loaded['ppm'] == pytest.approx(expected[3])


expected_load_two = [
    ( 129.1015 ,   16971, 'y1-H2O' ,  6.88),
    ( 147.112  ,   14367, 'y1'     ,  6.7 ),
    ( 275.171  ,   11000, 'y3'     ,  0.15),
    ( 403.2282 ,   11824, 'y5'     ,  4.36),
    ( 427.2304 ,   16602, 'b6'     , -0.95),
    ( 474.2686 ,   11755, 'y6'     , -3.41),
    ( 498.265  ,   32364, 'b7'     ,  4.11),
    ( 531.2892 ,   45789, 'y7'     , -0.46),
    ( 569.3044 ,   47595, 'b8'     , -0.67),
    ( 602.3274 ,   35911, 'y8'     , -2.31),
    ( 622.3348 ,    7740, 'b9-H2O' , -7.08),
    ( 640.3398 ,   61188, 'b9'     ,  1.81),
    ( 673.3653 ,   63316, 'y9'     , -3.41),
    ( 693.3564 ,    9870, 'b10-H2O', 15.85),
    ( 711.3842 ,   55565, 'b10'    , -8.74),
    ( 744.3979 ,   64833, 'y10'    ,  2.75),
    ( 745.3943 ,   19792, 'y21-H2O^2',4.14),
    ( 782.4222 ,   53814, 'b11'    , -7.98),
    ( 798.4098 , 1489354, 'y11-NH3',  0.83),
    ( 815.4368 ,   55331, 'y11'    ,  0.21),
    ( 853.4541 ,   48433, 'b12'    , -1.29),
    ( 886.4764 ,   78957, 'y12'    , -2.68),
    ( 924.489  ,   35949, 'b13'    ,  1.13),
    ( 957.5161 ,   36344, 'y13'    , -5.28),
    ( 995.5306 ,   25739, 'b14'    , -3.66),
    (1028.5431 ,   38406, 'y14'    ,  4.77),
    (1066.5657 ,   23222, 'b15'    , -1.57),
    (1224.6147 ,    8688, 'y17-NH3', 15.27),
    (1241.6492 ,    8863, 'y17'    ,  8.72),
]


@pytest.mark.msp_loader
def test_load_multiple():
    spectrumobject = SpectrumObject()

    with open('./data/multiple.msp') as f:
        msp_loader.load(spectrumobject, f)

        assert spectrumobject['sequence'] == 'AAAAAAAAAAAAAAAASAGGK'
        assert spectrumobject['charge'] == 2
        assert spectrumobject['MW'] == 1554.8117
        assert spectrumobject['num peaks'] == 27

        for loaded, expected in zip(spectrumobject['peaks'], expected_load_one):
            assert loaded['mz'] == pytest.approx(expected[0])
            assert loaded['I'] == expected[1]
            assert loaded['type'] == expected[2]
            assert loaded['ppm'] == pytest.approx(expected[3])

        msp_loader.load(spectrumobject, f)

        assert spectrumobject['sequence'] == 'AAAAAAAAAAAAAAAGAGAGAK'
        assert spectrumobject['charge'] == 2
        assert spectrumobject['MW'] == 1595.8377
        assert spectrumobject['num peaks'] == 29

        for loaded, expected in zip(spectrumobject['peaks'], expected_load_two):
            assert loaded['mz'] == pytest.approx(expected[0])
            assert loaded['I'] == pytest.approx(expected[1])
            assert loaded['type'] == expected[2]
            assert loaded['ppm'] == pytest.approx(expected[3])


@pytest.mark.msp_loader
def test_load_multiple_different():
    spectrumobject1 = SpectrumObject()
    spectrumobject2 = SpectrumObject()

    with open('./data/multiple.msp') as f:
        msp_loader.load(spectrumobject1, f)
        msp_loader.load(spectrumobject2, f)

        assert spectrumobject1['sequence'] == 'AAAAAAAAAAAAAAAASAGGK'
        assert spectrumobject1['charge'] == 2
        assert spectrumobject1['MW'] == 1554.8117
        assert spectrumobject1['num peaks'] == 27

        for loaded, expected in zip(spectrumobject1['peaks'], expected_load_one):
            assert loaded['mz'] == pytest.approx(expected[0])
            assert loaded['I'] == pytest.approx(expected[1])
            assert loaded['type'] == expected[2]
            assert loaded['ppm'] == pytest.approx(expected[3])


        assert spectrumobject2['sequence'] == 'AAAAAAAAAAAAAAAGAGAGAK'
        assert spectrumobject2['charge'] == 2
        assert spectrumobject2['MW'] == 1595.8377
        assert spectrumobject2['num peaks'] == 29

        for loaded, expected in zip(spectrumobject2['peaks'], expected_load_two):
            assert loaded['mz'] == pytest.approx(expected[0])
            assert loaded['I'] == pytest.approx(expected[1])
            assert loaded['type'] == expected[2]
            assert loaded['ppm'] == pytest.approx(expected[3])

