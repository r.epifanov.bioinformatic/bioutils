# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 14/01/2020

import pytest


from bioutils.io.spectra import msp_loader
from bioutils.structures import SpectrumObject


@pytest.fixture
def spectrumobject():
    return SpectrumObject()


@pytest.mark.spectrumobject
def test_getitem(spectrumobject):
    try:
        spectrumobject['sequence']
        spectrumobject['charge']
        spectrumobject['MW']
        spectrumobject['comment']
        spectrumobject['num peaks']
        spectrumobject['peaks']
    except:
        pytest.fail('SpectrumObject haven\'t required attributes')


expected_load_flatten = [
    ( 129.1028 ,   60343, 'y1-H2O' , -3.17),
    ( 147.1132 ,   99766, 'y1'     , -1.6 ),
    ( 295.0789 ,  331507, 'b2'     , -3.22),
    ( 442.1153 ,  481599, 'b3'     , -5.26),
    ( 531.3249 ,  546904, 'y4'     ,  0.21),
    ( 630.3988 ,  173927, 'y5'     , -9.21),
    ( 638.7397 , 1470632, 'b5'     , -1.9 ),
    ( 727.95145, 8886986, 'y6'     , -1.985),
    ( 737.80905,  678614, 'b6'     , -3.42),
    ( 842.97945, 1393656, 'y7'     , -3.475),
    ( 852.8366 , 1429501, 'b7'     , -2.985),
    ( 942.5534 ,  267468, 'y8'     , -5.67),
    ( 950.3872 ,  440506, 'b8'     ,  2.87),
    (1138.6734 , 1504984, 'y10'    , -2.93),
    (1285.7076 ,  656228, 'y11'    , -1.18),
    (1432.7432 ,  102505, 'y12'    , -2.21),
]


@pytest.mark.spectrumobject
def test_flatten(spectrumobject):
    with open('./data/double_charged.msp') as f:
        msp_loader.load(spectrumobject, f)

    spectrumobject.flatten()

    for loaded, expected in zip(spectrumobject['peaks'], expected_load_flatten):
        assert loaded['mz'] == pytest.approx(expected[0])
        assert loaded['I'] == pytest.approx(expected[1])
        assert loaded['type'] == expected[2]
        assert loaded['ppm'] == pytest.approx(expected[3])


expected_load_filter = [
    ( 129.1028 ,   60343, 'y1-H2O' , -3.17),
    ( 147.1132 ,   99766, 'y1'     , -1.6 ),
    ( 295.0789 ,  331507, 'b2'     , -3.22),
    ( 442.1153 ,  481599, 'b3'     , -5.26),
    ( 531.3249 ,  546904, 'y4'     ,  0.21),
    ( 630.3988 ,  173927, 'y5'     , -9.21),
    ( 638.2366 ,  873313, 'b5'     , -2.46),
    ( 727.4481 ,  966914, 'y6'     , -2.91),
    ( 737.3073 ,  362813, 'b6'     , -5.77),
    ( 842.4769 ,  313715, 'y7'     , -4.59),
    ( 852.3328 , 1242839, 'b7'     , -3.24),
]


@pytest.mark.spectrumobject
def test_filter(spectrumobject):
    with open('./data/double_charged.msp') as f:
        msp_loader.load(spectrumobject, f)

    print(spectrumobject['peaks'])
    spectrumobject.filter()
    print(spectrumobject['peaks'])

    for loaded, expected in zip(spectrumobject['peaks'], expected_load_filter):
        assert loaded['mz'] == pytest.approx(expected[0])
        assert loaded['I'] == pytest.approx(expected[1])
        assert loaded['type'] == expected[2]
        assert loaded['ppm'] == pytest.approx(expected[3])
