# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 14/01/2020

import pytest


from bioutils.structures import PeakObject


@pytest.fixture
def peakobject():
    return PeakObject()


@pytest.mark.peakobject
def test_getitem(peakobject):
    try:
        peakobject['I']
        peakobject['mz']
        peakobject['type']
        peakobject['ppm']
    except:
        pytest.fail('PeakObject haven\'t required attributes')
