# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 14/01/2020

import pytest


from bioutils.structures.spectra.object import BaseObject


class SimpleObject(BaseObject):
    __ATTRIBUTES = (('key_str', str, ''),
                    ('key_int', int, 0))

    def __init__(self, **kargs):
        BaseObject.__init__(self, **kargs)

        self._initattributes(self.__ATTRIBUTES, kargs)


@pytest.fixture
def simpleobject():
    return SimpleObject()


@pytest.mark.simpleobject
def test_getitem(simpleobject):
    assert simpleobject['key_str'] == ''
    assert simpleobject['key_int'] == 0

    with pytest.raises(LookupError):
        simpleobject['key_none']


@pytest.mark.simpleobject
def test_setitem(simpleobject):
    simpleobject['key_str'] = 'value'
    assert simpleobject['key_str'] == 'value'

    with pytest.raises(TypeError):
        simpleobject['key_str'] = None

    with pytest.raises(LookupError):
        simpleobject['key_none']
