# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 23/03/2020

AMINOACID2CODE = {
    'A': 1, 'R': 2, 'N': 3, 'D': 4,
    'C': 5, 'Q': 6, 'E': 7, 'G': 8,
    'H': 9, 'I': 10, 'L': 11, 'K': 12,
    'M': 13, 'F': 14, 'P': 15, 'S': 16,
    'T': 17, 'W': 18, 'Y': 19, 'V': 20
}

CODE2AMINOACID = {v: k for k, v in AMINOACID2CODE.items()}
