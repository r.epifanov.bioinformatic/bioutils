# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 23/03/2020

import numpy as np


from bioutils.vector.constants import AMINOACID2CODE, CODE2AMINOACID


__all__ = [
    'encode_dissociation',
    'decode_dissociation',
    'encode_sequence',
    'decode_sequence',
]


def encode_dissociation(type_):
    if 'CID' == type_:
        return 0
    elif 'HCD' == type_:
        return 1
    else:
        raise RuntimeError()


def decode_dissociation(code):
    if 0 == code:
        return 'CID'
    elif 1 == code:
        return 'HCD'
    else:
        raise RuntimeError()


def encode_sequence(sequence, size=None):
    if size is None:
        size = len(sequence)
    elif size < 0:
        raise RuntimeError()


    code = np.zeros(size, dtype=np.int32)

    for idx, char in enumerate(sequence):
        code[idx] = AMINOACID2CODE[char]

    return code


def decode_sequence(vector):
    sequence = ''

    for code in vector:
        if code:
            sequence += CODE2AMINOACID[code]

    return sequence
