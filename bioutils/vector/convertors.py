# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 14/09/2020

import numpy as np


__all__ = [
    'get_vectored_spectrum',
]


def get_vectored_spectrum(info, vsize=None):
    if vsize is None:
        vsize = len(info['sequence']) - 1
    elif vsize + 1 < len(info['sequence']):
        vsize = len(info['sequence']) - 1

    size = len(info['sequence'])

    y_intensity = np.zeros(vsize)
    b_intensity = np.zeros(vsize)
    imask = np.zeros(vsize)

    imask[:size-1] = np.ones(size-1)

    for type_, I in info['spectrum']['1+']:
        pos = int(type_[1:])

        if type_[0] == 'y':
            pos = size - pos
            y_intensity[pos-1] = I
        else:
            b_intensity[pos-1] = I

    return b_intensity, y_intensity, imask
