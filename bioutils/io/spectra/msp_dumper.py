# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 20/12/2019

import re


from bioutils.structures import PeakObject
from bioutils.errors import SpectrumEncodeError


__all__ = ['dump']


def __get_type_prefix(type_):
    if type_ == 'TYPE_NAME':
        return 'Name: '
    elif type_ == 'TYPE_MW':
        return 'MW: '
    elif type_ == 'TYPE_CMT':
        return 'Comment: '
    elif type_ == 'TYPE_PEAKS':
        return 'Num peaks: '
    else:
        raise SpectrumEncodeError()


def __encode(spobj, type_, ostream):
    line = __get_type_prefix(type_)

    if type_ == 'TYPE_NAME':
        sequence = spobj['sequence']
        charge = str(spobj['charge'])
        value = sequence + '/' + charge
    elif type_ == 'TYPE_MW':
        MW = str(spobj['MW'])
        value = MW
    elif type_ == 'TYPE_CMT':
        comment = spobj['comment']
        value = comment
    elif type_ == 'TYPE_PEAKS':
        num_peaks = str(spobj['num peaks'])
        value = num_peaks
    else:
        raise SpectrumEncodeError()

    line += value
    line += '\n'
    ostream.write(line)

    if type_ == 'TYPE_PEAKS':
        for peak in spobj['peaks']:
            value = str(peak) + '\n'
            ostream.write(value)


def dump(spobj, ostream):
    format_structure = ['TYPE_NAME', 'TYPE_MW', 'TYPE_CMT', 'TYPE_PEAKS']

    for type_ in format_structure:
        __encode(spobj, type_, ostream)

    ostream.write('\n')
