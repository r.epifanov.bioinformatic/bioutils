# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 20/12/2019

import re


from bioutils.structures import PeakObject
from bioutils.errors import SpectrumDecodeError


__all__ = ['load']


def __get_line_type(line):
    line = line.upper()
    if line.startswith('BEGIN IONS'):
        return 'TYPE_SYSTEM_BEGIN'
    elif line.startswith('PEPMASS='):
        return 'TYPE_MW'
    elif line.startswith('CHARGE='):
        return 'TYPE_CHARGE'
    elif line.startswith('END IONS'):
        return 'TYPE_SYSTEM_END'
    elif line.startswith('SEQ='):
        return 'TYPE_SEQ'
    elif re.match('\w+=', line):
        return 'TYPE_CMT'
    elif not line.startswith('='):
        return 'TYPE_PEAK'
    else:
        raise SpectrumDecodeError()


def __readline(istream):
    line = istream.readline()

    if line:
        line = line.strip('\n')
        return line
    else:
        raise EOFError()


def __decode(spobj, line, type_, istream):
    if type_ == 'TYPE_SYSTEM_BEGIN':
        pass
    elif type_ == 'TYPE_MW':
        tokens = line.split('=')
        tokens = tokens[1].split()
        value = tokens[0]
        
        spobj['MW'] = float(value)
    elif type_ == 'TYPE_CMT':
        tokens = line.split('=')
        value = '='.join(tokens[1:])

        if spobj['comment'] != '':
            spobj['comment'] += ' '

        spobj['comment'] += str(value)
    elif type_ == 'TYPE_CHARGE':
        tokens = line.split('=')
        value = tokens[1]

        if value[-1] == '+':
            value = value[:-1]

        spobj['charge'] = int(value)    
    elif type_ == 'TYPE_SEQ':
        tokens = line.split('=')
        value = tokens[1]

        spobj['sequence'] = str(value)
    elif type_ == 'TYPE_PEAK':
        tokens = line.split()
        mz = float(tokens[0])
        I = float(tokens[1])

        type_ = ''
        if len(tokens) == 3:
            template1 = re.compile('(.*)\((\d+)\)')

            match1 = re.match(template1, tokens[2])

            if match1:
                type_ = match1.group(1)
                num = match1.group(2)

                if len(type_) == 2:
                    type_ = type_[0] + num + type_[2:] + '^' + type_[1]
                else:
                    type_ = type_[0] + num + type_[1:]


        peak = PeakObject(I=I, mz=mz, type=type_)
        spobj['peaks'].append(peak)
    elif type_ == 'TYPE_SYSTEM_END':
        spobj['peaks'] = spobj['peaks']
    else:
        raise SpectrumDecodeError()


def load(spobj, istream):
    line = __readline(istream)

    while line:
        type_ = __get_line_type(line)
        __decode(spobj, line, type_, istream)

        line = __readline(istream)
