# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 20/12/2019

import re


from bioutils.structures import PeakObject
from bioutils.errors import SpectrumDecodeError


__all__ = ['load']


def __get_line_type(line):
    if line.startswith('Name: '):
        return 'TYPE_NAME'
    elif line.startswith('MW: '):
        return 'TYPE_MW'
    elif line.startswith('Comment: '):
        return 'TYPE_CMT'
    elif line.startswith('Num peaks: '):
        return 'TYPE_PEAKS'
    else:
        raise SpectrumDecodeError()


def __decode(spobj, line, type_, istream):
    if type_ == 'TYPE_NAME':
        tokens = line.split()
        value = tokens[1]

        sequence, charge = value.split('/')
        spobj['sequence'] = sequence
        spobj['charge'] = int(charge)
    elif type_ == 'TYPE_MW':
        tokens = line.split()
        value = tokens[1]

        spobj['MW'] = float(value)
    elif type_ == 'TYPE_CMT':
        spobj['comment'] = line
    elif type_ == 'TYPE_PEAKS':
        tokens = line.split()
        value = tokens[2]

        spobj['peaks'].clear()
        spobj['num peaks'] = int(value)

        for _ in range(spobj['num peaks']):
            line = istream.readline()

            tokens = line.split()
            mz = float(tokens[0])
            I = float(tokens[1])

            value = tokens[2]


            template1 = re.compile('"(.*)/(.*)ppm"')
            template2 = re.compile('"(.*)"')
            template3 = re.compile('(.*)/(.*)')

            match1 = re.match(template1, value)
            match2 = re.match(template2, value)
            match3 = re.match(template3, value)

            if match1:
                ion_type = str(match1.group(1))
                ppm = float(match1.group(2))
            elif match2:
                ion_type = str(match2.group(1))
                ppm = float(0.0)
            elif match3:
                ion_type = str(match3.group(1))
                ppm = float(match3.group(2))
            else:
                raise AssertionError('Unknown template file for msp')


            ion_type = ion_type[0].lower() + ion_type[1:]
            peak = PeakObject(I=I, mz=mz, type=ion_type, ppm=ppm)
            spobj['peaks'].append(peak)
    else:
        raise SpectrumDecodeError()


def __readline(istream):
    line = istream.readline()

    if line:
        line = line.strip('\n')
        return line
    else:
        raise EOFError()


def load(spobj, istream):
    line = __readline(istream)

    while line:
        type_ = __get_line_type(line)
        __decode(spobj, line, type_, istream)

        line = __readline(istream)
