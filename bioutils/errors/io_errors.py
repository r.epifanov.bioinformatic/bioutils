# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 30/10/2019


class SpectrumDecodeError(Exception):
    pass


class SpectrumEncodeError(Exception):
    pass
