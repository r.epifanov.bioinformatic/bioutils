# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 14/01/2020

import typing as t


class BaseObject(object):
    def __init__(self, **kargs):
        self.__inner_structure = dict()


    def __inititem(self, key, value):
        self.__inner_structure[key] = value


    def _initattributes(self, attributes, kwargs):
        for name, type_, default in attributes:
            if isinstance(kwargs.get(name), type_):
                self.__inititem(name, kwargs.get(name))
            else:
                if isinstance(default, t.Callable):
                    self.__inititem(name, default())
                else:
                    self.__inititem(name, default)


    def __getitem__(self, key):
        if key in self.__inner_structure:
            return self.__inner_structure[key]
        else:
            raise LookupError()


    def __setitem__(self, key, value):
        if key not in self.__inner_structure:
            raise LookupError()

        if type(value) is not type(self.__inner_structure[key]):
            raise TypeError()

        self.__inner_structure[key] = value
