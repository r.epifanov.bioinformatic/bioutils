# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 20/12/2019

import re


from bioutils.structures.spectra.object import BaseObject
from bioutils.structures import PeakObject


class SpectrumObject(BaseObject):
    __ATTRIBUTES = (('sequence', str, ''),
                    ('charge', int, 0),
                    ('MW', float, 0.0),
                    ('comment', str, ''),
                    ('num peaks', int, 0),
                    ('peaks', list, list))


    def __init__(self, **kargs):
        BaseObject.__init__(self, **kargs)

        self._initattributes(self.__ATTRIBUTES, kargs)


    def __setitem__(self, key, value):
        BaseObject.__setitem__(self, key, value)

        if key == 'peaks':
            for ivalue in value:
                if not isinstance(ivalue, PeakObject):
                    raise TypeError()

            BaseObject.__setitem__(self, 'num peaks', len(value))

    
    def __repr__(self):
        representation = ''
        format_ = '{}: {}\n'

        for key, _, _ in self.__ATTRIBUTES:
            representation += format_.format(key, self[key])

        return representation


    def flatten(self):
        grouped_peaks = dict()

        for peak in self['peaks']:
            pos = peak['type'].find('^')

            if pos == -1:
                type_ = peak['type']
            else:
                type_ = peak['type'][:pos]
                charge = int(peak['type'][pos+1:])
                peak['mz'] = peak['mz'] * charge

            if type_ in grouped_peaks:
                grouped_peaks[type_].append(peak)
            else:
                grouped_peaks[type_] = [peak]

        flatten_peaks = list()

        for type_, peaks in grouped_peaks.items():
            I = sum([peak['I'] for peak in peaks])
            mz = sum([peak['mz'] for peak in peaks]) / sum([ 1 for peak in peaks])
            ppm = sum([peak['ppm'] for peak in peaks]) / sum([ 1 for peak in peaks])

            peak = PeakObject(I=I, mz=mz, type=type_, ppm=ppm)
            flatten_peaks.append(peak)

        flatten_peaks = sorted(flatten_peaks, key = lambda peak: peak['mz'])
        self['peaks'] = flatten_peaks


    def filter(self, filter=1):
        filtred_peaks = list()

        for peak in self['peaks']:
            pos = peak['type'].find('^')

            if pos == -1:
                type_ = peak['type']
                filtred_peaks.append(peak)
            else:
                type_ = peak['type'][:pos]
                charge = int(peak['type'][pos+1:])

                if filter == charge:
                    filtred_peaks.append(peak)

        filtred_peaks = sorted(filtred_peaks, key = lambda peak: peak['mz'])
        self['peaks'] = filtred_peaks
