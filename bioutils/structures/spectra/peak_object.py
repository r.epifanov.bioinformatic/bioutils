# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 14/01/2020

import re


from bioutils.structures.spectra.object import BaseObject


class PeakObject(BaseObject):
    __ATTRIBUTES = (('I', float, 0.0),
                    ('mz', float, 0.0),
                    ('type', str, ''),
                    ('ppm', float, 0.0))


    def __init__(self, **kargs):
        BaseObject.__init__(self, **kargs)

        self._initattributes(self.__ATTRIBUTES, kargs)


    def __repr__(self):
        representation = ''

        representation += '<'
        format_ = '{}: {}, '

        for key, _, _ in self.__ATTRIBUTES:
            representation += format_.format(key, self[key])

        representation = representation[:-2]

        representation += '>'
        return representation


    def __str__(self):
        representation = ''

        representation += str(self['mz'])
        representation += '\t'

        representation += str(self['I'])
        representation += '\t'

        ppm = self['type'] + '/' + str(self['ppm']) + 'ppm'
        representation += '"' + ppm + '"'
        return representation
