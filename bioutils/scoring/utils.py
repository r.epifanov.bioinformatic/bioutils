# Copyrights rostepifanov.ru
#   Author: Rostislav Epifanov
#   Created: 02/02/2020


import numpy as np
from sklearn.metrics import auc
from scipy.stats.stats import pearsonr
from sklearn.metrics.pairwise import cosine_similarity

def get_xy(corrs):
    x = np.arange(0, 1, 0.0001)
    y = list()

    for xi in x:
        yi = corrs[corrs < xi].size / corrs.size * 100
        y.append(yi)

    y = np.array(y)
    
    return x, y


def science_metric(x, y):
    ids = (0. <= x) & (x <= 1.)
    xcrop = x[ids]
    ycrop = y[ids]

    metric = auc(xcrop, ycrop) / 100

    return metric


def generate_corrs(y_true, y_pred, masks):
    corrs = list()

    for true, pred, mask in zip(y_true, y_pred, masks):
        true = true[:mask]
        pred = pred[:mask]

        corr = pearsonr(true, pred)[0]
        # corr = cosine_similarity([true], [pred])[0][0]

        corrs.append(corr)

    corrs = sorted(corrs)
    corrs = np.array(corrs)

    return corrs
